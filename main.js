/*
    STEP 1

    For the page we need the following JavaScript functions:

    Show today's date in the following format: Today is Tuesday 12th June 2018
    Show the current time in the following format: 10.30 AM
    Show today's task from this array of task in a table with Time and Description
    Change page title dynamically on button press from free text in a input field
    Change page background colour with a drop down of different colours
*/

// Show today's date in the following format: Today is Tuesday 12th June 2018

const days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday ",
  "Thursday",
  "Friday",
  "Saturday"
];
const dateDiv = document.getElementById("day");
const today = new Date();
console.log("today ", today);
const currentMonth = today.toLocaleString("en-us", { month: "long" });
const day = today.getDay();
const year = today.getFullYear();

const dayOfTheWeek = today => {
  return (
    today.getDate() +
    (today.getDate() % 10 == 1 && today.getDate() != 11
      ? "st"
      : today.getDate() % 10 == 2 && today.getDate() != 12
      ? "nd"
      : today.getDate() % 10 == 3 && today.getDate() != 13
      ? "rd"
      : "th")
  );
};

const fullDate = `${days[day]} ${dayOfTheWeek(today)} ${currentMonth} ${year}`;

dateDiv.innerHTML = fullDate;

// Show the current time in the following format: 10.30 AM
const timeDiv = document.getElementById("current-time");
const hour = today.getHours();
const minutes = today.getMinutes();
const minute = minutes < 10 ? "0" + today.getMinutes() : today.getMinutes();
console.log("minute ", minute);
const timeNow =
  hour < 12 ? hour + ":" + minute + " AM" : hour + ":" + minute + " PM";

timeDiv.innerHTML = timeNow;

// Show today's task from this array of task in a table with Time and Description
fetch("./tasks.json")
  .then(function(response) {
    return response.json();
  })
  .then(function(tasks) {
    console.log(tasks);
    tasks.forEach(function(task) {
      const dayOfTheWeek = task.day;
      const time = task.time;

      if (dayOfTheWeek === days[day]) {
        var todos = [{ time: task.time, description: task.description }];
        var parentToDoDiv = document.createElement("div");
        parentToDoDiv.className = "to-do-tasks";
        var taskTime = document.createElement("p");
        var taskDescription = document.createElement("p");
        taskTime.innerHTML += task.time;
        taskDescription.innerHTML += task.description;
        parentToDoDiv.appendChild(taskTime);
        parentToDoDiv.appendChild(taskDescription);
        document.getElementById("toDoTask").appendChild(parentToDoDiv);
      }
    });
  });

//Change page title dynamically on button press from free text in a input field
const btnChangePageTitle = document.getElementById("btnChangePageTitle");
btnChangePageTitle.addEventListener("click", event => {
  const inputElement = document.getElementById("txtPageTitle").value;
  document.getElementById("displayPageTitle").innerHTML = inputElement;
});

//Change page background colour with a drop down of different colours
function changeColor(el) {
  document.body.style.backgroundColor = el.value;
}

/*
    STEP 3 - ADVANCED

    Want to show us more? You can choose to keep your sample as it is or you can choose to show us what more you can do. We have the following suggestions:

    Make a tab to show tomorrow’s tasks
    Show the current weather
    Make the page responsive in all sizes
    Add animations
    Make CRUD functionality for the task list. (Just enough to modify the array, no need for storage or to save it after a refresh)
    
    
    Show inspirational message of the day

*/
